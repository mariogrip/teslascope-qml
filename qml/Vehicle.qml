/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.10
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Qt.labs.settings 1.0

import "./components"

import "js/api.js" as Api
import "js/units.js" as Units

Page{
    id: mainPage
    anchors.fill: parent

    property var httpData: false
    property alias settings: settingsComponent

    Settings {
        id: settingsComponent
        property var apikey;
        property var vehicleId;
        property var metric;
        property var celsius;
    }

    property alias apikey: settingsComponent.apikey;
    property alias vehicleId: settingsComponent.vehicleId;
    property alias metric: settingsComponent.metric
    property alias celsius: settingsComponent.celsius

    property var vehicleApi: new Api.Vehicle(settings.apikey, settings.vehicleId)

    function update(state, callback) {
        if (!apikey || !vehicleId) {
            settingsOpener.start()
            return;
        }

        if (state == "loading") {
            console.log(settings.vehicleId)
            mainPage.vehicleApi = new Api.Vehicle(settings.apikey, settings.vehicleId);            
        }

        mainPage.state = state;
        vehicleApi.infomation((a) => {
            if (a.code != "200") {
                errorText.text = a.response;
                mainPage.state = "error";
            } else {
                mainPage.httpData = a.response;
                mainPage.state = "ready";
            }

            if (callback)
                callback(a);
        })
    }

    Component.onCompleted: mainPage.update("loading")

    Timer {
        id: settingsOpener
        interval: 10
        running: false
        onTriggered: {
            adaptivePageLayout.addPageToCurrentColumn(mainPage, Qt.resolvedUrl("SettingsPage.qml"), {mainPage: mainPage})
        }
    }

    header: PageHeader {
        title: i18n.tr("Teslascope")
        flickable: flickable

        // the bar on the left side 
        leadingActionBar.actions: [
            Action {
                id: updatesPageBtn
                iconName: "preferences-system-updates-symbolic"
                text: i18n.tr("Updates")
                onTriggered:{
                    adaptivePageLayout.addPageToNextColumn(mainPage, Qt.resolvedUrl("Updates.qml"), {carVersion: mainPage.httpData.car_version})
                }
            },
            Action {
                id: aboutProductPage
                iconName: "settings"
                text: i18n.tr("Settings")
                onTriggered:{
                     adaptivePageLayout.addPageToNextColumn(mainPage, Qt.resolvedUrl("SettingsPage.qml"), {mainPage: mainPage})
                }
            }
        ]
    }

    states: [
        State {
            name: "loading"
            PropertyChanges { target: loadingIndicator; running: true}
            PropertyChanges { target: content; visible: false}
            PropertyChanges { target: pullToRefresh; enabled: false; refreshing: false}
            PropertyChanges { target: error; visible: false;}
        },
        State {
            name: "ready"
            PropertyChanges { target: loadingIndicator; running: false}
            PropertyChanges { target: content; visible: true}
            PropertyChanges { target: pullToRefresh; enabled: true; refreshing: false}
            PropertyChanges { target: error; visible: false;}
        },
        State {
            name: "reloading"
            PropertyChanges { target: loadingIndicator; running: false}
            PropertyChanges { target: content; visible: true;}
            PropertyChanges { target: pullToRefresh; enabled: true; refreshing: true}
            PropertyChanges { target: error; visible: false;}
        },
        State {
            name: "error"
            PropertyChanges { target: loadingIndicator; running: false}
            PropertyChanges { target: content; visible: false;}
            PropertyChanges { target: pullToRefresh; enabled: false; refreshing: false}
            PropertyChanges { target: error; visible: true;}
        }
    ]

    ActivityIndicator {
        id: loadingIndicator

        width: units.gu(3)
        height: units.gu(3)

        anchors.centerIn: parent
        running: true
        visible: running
    }

    Column {
        id: error
        width: parent.width
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        spacing: units.gu(2)

        Label {
            textSize: Label.XLarge
            anchors.horizontalCenter: parent.horizontalCenter
            text: ":("
        }

        Label {
            id: errorText
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Error"
        }

        Row {
            width: parent.width
            Button {
                text: "Try again"
                onClicked: mainPage.update("reloading")
                width: parent.width/2
                color: UbuntuColors.green
            }

            Button {
                text: "Open settings"
                onClicked: adaptivePageLayout.addPageToCurrentColumn(mainPage, Qt.resolvedUrl("SettingsPage.qml"), {mainPage: mainPage})
                width: parent.width/2
            }
        }
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > root.height) ? Flickable.DragAndOvershootBounds : Flickable.StopAtBounds
        /* Set the direction to workaround https://bugreports.qt-project.org/browse/QTBUG-31905
           otherwise the UI might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        PullToRefresh {
            id: pullToRefresh
            parent: flickable
            refreshing: false
            onRefresh: mainPage.update("reloading")
        }

        Column {
            id: content

            anchors.left: parent.left
            anchors.right: parent.right
            
            visible: false
            enabled: visible

            Column {
                id: about

                anchors.left: parent.left
                anchors.right: parent.right

                spacing: 6
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    topPadding: units.gu(1)
                    bottomPadding: units.gu(1)
                    spacing: 4

                    height: Math.max(lockedIcon.height, nameLabel.height)

                    Label {
                        id: nameLabel
                        text: mainPage.httpData.name
                        textSize: Label.Large
                    }
                    Icon {
                        id: lockedIcon
                        width: units.gu(3)
                        height: width
                        name: httpData.vehicle.locked ? "lock" : "lock-broken" // TODO We dont have a good unlocked icon yet
                        SlotsLayout.position: SlotsLayout.Last
                    }
                }

                Label {
                    text: `${mainPage.httpData.model} ${mainPage.httpData.trim} (${mainPage.httpData.year})`
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Image {
                    width: Math.min(parent.width, units.gu(50))
                    source: mainPage.httpData.render_url + "?api_key=" + apikey
                    fillMode: Image.PreserveAspectFit
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            ListItem.ThinDivider {}

            ListButton {
                text: "Controls"
                Icon {
                    width: units.gu(3)
                    height: width
                    name: "audio-carkit-symbolic"
                    SlotsLayout.position: SlotsLayout.First
                }
                onClicked: {
                    adaptivePageLayout.addPageToNextColumn(mainPage, Qt.resolvedUrl("Controls.qml"), {mainPage: mainPage, apikey: apikey, vehicleId: vehicleId, httpData: httpData})
                }
            }

            Column {
                id: battery

                anchors.left: parent.left
                anchors.right: parent.right

                ItemTitle {
                    id: batteryItem
                    objectName: "batteryItem"
                    text: i18n.tr("Battery:")
                }

                state: mainPage.httpData.battery.charging_state
                states: [
                    State {
                        name: "Charging"
                        PropertyChanges { target: stateValue; value: "Charging"; visible: true }
                        PropertyChanges { target: chargingTimeValue; visible: true }
                    },
                    State {
                        name: "Disconnected"
                        PropertyChanges { target: stateValue; value: ""; visible: false }
                        PropertyChanges { target: chargingTimeValue; visible: false }
                    },
                    State {
                        name: "Stopped"
                        PropertyChanges { target: stateValue; value: "Plugged in / Stopped"; visible: true }
                        PropertyChanges { target: chargingTimeValue; visible: false }
                    }
                ]

                SingleValue {
                    id: stateValue
                    text: "State"
                    value: ""

                    Icon {
                        width: units.gu(3)
                        height: width
                        name: Units.toBatIcon(mainPage.httpData.battery.level, mainPage.httpData.battery.charging_state == "Charging") // TODO
                    }
                }

                SingleValue {
                    text: "Range"
                    value: `${Units.mToKmIf(mainPage.httpData.battery.range, mainPage.metric)} (${mainPage.httpData.battery.level}%)`
                }

                SingleValue {
                    id: chargingTimeValue
                    text: "Estimated charging time"
                    value: `${mainPage.httpData.battery.time_remaining} hours`
                }
            }

            Column {
                id: climate

                anchors.left: parent.left
                anchors.right: parent.right

                ItemTitle {
                    id: climateItem
                    objectName: "climateItem"
                    text: i18n.tr("Climate:")
                }

                ListButtonValue {
                    text: "Inside"
                    value: `${Units.fToCIf(mainPage.httpData.climate.inside, mainPage.celsius)}`
                    onClicked: {
                        adaptivePageLayout.addPageToNextColumn(mainPage, Qt.resolvedUrl("Climate.qml"), {apikey: apikey, vehicleId: vehicleId, httpData: httpData})
                    }
                    Icon {
                        width: units.gu(3)
                        height: width
                        name: "weather-chance-of-wind"
                        SlotsLayout.position: SlotsLayout.First
                    }
                }
                SingleValue {
                    text: "Outside"
                    value: `${Units.fToCIf(mainPage.httpData.climate.outside, mainPage.celsius)}`
                    Icon {
                        width: units.gu(3)
                        height: width
                        name: "weather-chance-of-snow"
                        SlotsLayout.position: SlotsLayout.First
                    }
                }
            }

            Column {
                id: software

                anchors.left: parent.left
                anchors.right: parent.right

                ItemTitle {
                    id: softwareItem
                    objectName: "softwareItem"
                    text: i18n.tr("Software:")
                }

                ListButtonValue {
                    text: "Version"
                    value: mainPage.httpData.car_version
                    onClicked: {
                        adaptivePageLayout.addPageToNextColumn(mainPage, Qt.resolvedUrl("Updates.qml"), {carVersion: mainPage.httpData.car_version})
                    }
                    Icon {
                        width: units.gu(3)
                        height: width
                        name: "preferences-system-updates-symbolic"
                        SlotsLayout.position: SlotsLayout.First
                    }
                }
            }

            Column {
                id: statistics

                anchors.left: parent.left
                anchors.right: parent.right

                ItemTitle {
                    id: statisticsItem
                    objectName: "StatisticsItem"
                    text: i18n.tr("Statistics:")
                }

                SingleValue {
                    text: "drives"
                    value: mainPage.httpData.statistics.drives
                }
                SingleValue {
                    text: "distance"
                    value: `${Units.mToKmIf(mainPage.httpData.statistics.distance, mainPage.metric)}`
                }
                SingleValue {
                    text: "charges"
                    value: `${mainPage.httpData.statistics.charges} (supercharges: ${mainPage.httpData.statistics.supercharging})`
                }
                SingleValue {
                    text: "joined"
                    value: mainPage.httpData.statistics.joined
                }
                SingleValue {
                    text: "hw"
                    value: mainPage.httpData.statistics.hw
                }
            }
        }
    }
}
