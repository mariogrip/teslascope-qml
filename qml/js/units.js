/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function fToCIf(f, i) {
    return i ? `${fToC(f)}ºC` : `${f}ºF`;
}

function mToKmIf(m, i) {
    return i ? `${mToKm(m)} Kilometers` : `${m} Miles`;
}

function fToC(f) {
    return Math.floor((f - 32) * 5 / 9)
}

function mToKm(m) {
    return (m * 1.60934).toFixed(2);
}

function toBatIcon(per, charging) {
    var nearestTen = Math.round(per / 10);
    if (nearestTen == 10)
        var bat = `${nearestTen}0`
    else
        var bat = `0${nearestTen}0`

    return `battery-${bat}${charging ? "-charging" : ""}`
}
