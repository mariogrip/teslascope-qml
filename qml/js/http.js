/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Keep it simple stupid
Super light http library

Made to be compatible with QML and (most) Browsers
for this reason callback is used instead of promises 

Usage:

request.get(url, callback);
request.post(url, data, callback);


callback returns a object
{
    status: [Status Code],
    text: [Response in text],
    json: Function to return text in parsed as json
}

This follows the http standard where functions are crafted with
that in mind, if you need to call a non standard method
use request.raw()


    Component.onCompleted: {
        
        Http.request.get("https://teslascope.com/api/software", (a) => {
            console.log(a.text);
            console.log(JSON.stringify(a.headers));
        })
    }

*/

function rawRequest(method, url, data, callback) {   
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = (function(myxhr) {
        return function() {
            callback(myxhr);
        }
    })(xhr);
    xhr.open(method, url, true);
    xhr.send(data);
}

function rawHeaderParse(xhr) {
    // Get the raw header string
    var headers = xhr.getAllResponseHeaders();

    // Convert the header string into an array
    // of individual headers
    var arr = headers.trim().split(/[\r\n]+/);

    // Create a map of header names to values
    var headerMap = {};
    arr.forEach(function (line) {
        var parts = line.split(': ');
        var header = parts.shift();
        var value = parts.join(': ');
        headerMap[header] = value;
    });
    return headerMap;
}

function rawStatusResponse(xhr, callback) {
    if(xhr.readyState === XMLHttpRequest.DONE) {
        callback({
            status: xhr.status,
            headers: rawHeaderParse(xhr)
        });
    }
}

function rawResponse(xhr, callback) {
    if(xhr.readyState === XMLHttpRequest.DONE) {
        var status = xhr.status;
        if (status === 0 || (status >= 200 && status < 400)) {
            callback({
                status: xhr.status,
                text: xhr.responseText,
                headers: rawHeaderParse(xhr),
                json: () => {
                    var js = JSON.parse(xhr.responseText);
                    console.log(js);
                    return js
                }
            });
        } else {
            callback({
                status: xhr.status,
                text: "",
                headers: {},
                json: () => { return {}}
            });
        }
    }
}

const request = {
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET
    get: (url, callback) => {
        rawRequest("GET", url, '', (xhr) => { rawResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
    post: (url, data, callback) => {
        rawRequest("POST", url, data, (xhr) => { rawResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD
    head: (url, callback) => {
        rawRequest("HEAD", url, (xhr) => { rawStatusResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT
    PUT: (url, data, callback) => {
        rawRequest("PUT", url, data, (xhr) => { rawStatusResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE
    detete: (url, data, callback) => {
        rawRequest("DELETE", url, data, (xhr) => { rawResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/CONNECT
    connect: (url, callback) => {
        rawRequest("CONNECT", url, data, (xhr) => { rawResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS
    options: (url, callback) => {
        rawRequest("OPTIONS", url, "", (xhr) => { rawResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/TRACE
    trace: (url, callback) => {
        rawRequest("TRACE", url, (xhr) => { rawStatusResponse(xhr, callback); });
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH
    patch: (url, data, callback) => {
        rawRequest("PATCH", url, data, (xhr) => { rawResponse(xhr, callback); });
    },
    raw: (method, url, data, callback) => {
        rawRequest(method, url, data, (xhr) => { rawResponse(xhr, callback); });
    }
}
