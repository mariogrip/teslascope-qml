/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Keep it simple stupid
Super light teslascope api library

Made to be compatible with QML and (most) Browsers
for this reason callback is used instead of promises.

This library is made to be as simple and performant as possable,
this avoid any unessary operation that can slow down
applications using this.

*/

// Qt seems to refuse to import mjs, qt bug?
//import * as Http from "http.mjs";
.import "http.js" as Http

const DEFAULT_ENDPOINT = "https://teslascope.com/"

const CONTROL_COMMANDS = {
    honkHorn: "honkHorn",
    flashLights: "flashLights",
    enableSentryMode: "enableSentryMode",
    disableSentryMode: "disableSentryMode",
    startAC: "startAC",
    stopAC: "stopAC",
    ventWindows: "ventWindows",
    closeWindows: "closeWindows",
    openTrunk: "openTrunk",
    openFrunk: "openFrunk",
    openChargeDoor: "openChargeDoor",
    closeChargeDoor: "closeChargeDoor",
    startCharging: "startCharging",
    stopCharging: "stopCharging",
    unlock: "unlock",
    lock: "lock",
    triggerHomeLink: "triggerHomeLink"
}

// Class based
class Teslascope {
    constructor(apikey, endpoint) {
        if (apikey == "none") {
            this.apikey = "";
        }else {
            if (typeof apikey != "string")
                console.error("Api key not provided");
            this.apikey = `?api_key=${apikey}`;
        }

        if (typeof endpoint == "String" && endpoint.startsWith("http")) {
            this.endpoint = endpoint;
            console.log(`Using endpoint: ${this.endpoint}`);
        } else {
            this.endpoint = DEFAULT_ENDPOINT;
            console.log(`Using default endpoint: ${this.endpoint}`);
        }

        if (!this.endpoint.endsWith("/"))
            this.endpoint =+ "/";
    }

    call(path, args, callback) {
        if (Array.isArray(args))
            args = args.join("/");

        console.log(`Http get: ${this.endpoint}api/${path}/${args}?api_key=[KEY]`);
        Http.request.get(`${this.endpoint}api/${path}/${args}${this.apikey}`, (a) => {
            callback(a.json());
        });
    }
}

class Vehicle extends Teslascope {
    constructor(apikey, publicId) {
        super(apikey);

        if (typeof publicId != "string")
            console.error("Vehicle publicId not provided");
        this.publicId = publicId;
    }

    infomation(callback) {
        console.log("get info");
        this.call("vehicle", this.publicId, callback);
    }

    detaliedInfomationm(callback) {
        this.call("vehicle", [this.publicId, "detailed"], callback);
    }

    command(cmd, callback) {
        if (!cmd in CONTROL_COMMANDS) {
            console.error(`Command ${cmd} does not exist`);
            return callback({code: 500});
        }

        this.call("vehicle", [this.publicId, "command", cmd], callback);
    }

    update(callback) {
        this.call("vehicle", [this.publicId, "update"], callback);
    }

    state(callback) {
        this.call("vehicle", [this.publicId, "state"], callback);
    }

    wakeup(callback) {
        this.call("vehicle", [this.publicId, "wakeup"], callback);
    }
}

class Updates extends Teslascope {
    constructor(endpoint) {
        super("none", endpoint);
    }

    all(callback) {
        this.call("software", [], callback);
    }

    get(version, callback) {
        this.call("software", version, callback);
    }
}
