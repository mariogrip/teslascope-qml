/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.10
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import "./components"

import "js/api.js" as Api

Page{
    id: controlsPage
    anchors.fill: parent

    // Set from outside
    property var httpData: false
    property var apikey
    property var vehicleId

    property var vehicleApi: new Api.Vehicle(apikey, vehicleId)

    property var mainPage;

    function commandCall(cmd, callback) {
        state = "loading"
        controlsPage.vehicleApi.command(cmd, (a) => {
            // We call the parent view to update, this
            // way we make sure we are always in sync
            mainPage.update("reloading", (b) => {
                httpData = b.response
                state = "ready"
                topPopdown.show(a.response)
                if (callback) callback()
            })

        });
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Controls')
        flickable: flickable
        z: 2

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: "Back"
                onTriggered: removePages(controlsPage)
            }
        ]
    }

    TopPopdown {
        id: topPopdown 
    }

    state: "ready"
    states: [
        State {
            name: "loading"
            PropertyChanges { target: loadingIndicator; running: true}
            PropertyChanges { target: content; visible: false}
        },
        State {
            name: "ready"
            PropertyChanges { target: loadingIndicator; running: false}
            PropertyChanges { target: content; visible: true}
        }
    ]

    ActivityIndicator {
        id: loadingIndicator

        width: units.gu(3)
        height: units.gu(3)

        anchors.centerIn: parent
        running: false
        visible: running
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > root.height) ? Flickable.DragAndOvershootBounds : Flickable.StopAtBounds
        /* Set the direction to workaround https://bugreports.qt-project.org/browse/QTBUG-31905
           otherwise the UI might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        Column {
            anchors.left: parent.left
            anchors.right: parent.right

            id: content

            ListItem.Standard {
                id: lockSwitch
                text: i18n.tr("Lock")
                control: Switch {
                    id: lockSwitchCheck
                    checked: httpData.vehicle.locked
                    onTriggered: {
                        const cmd = httpData.vehicle.locked ? 
                                    Api.CONTROL_COMMANDS.unlock : Api.CONTROL_COMMANDS.lock;
                        commandCall(cmd, () => {
                            checked = httpData.vehicle.locked;
                        })
                    }
                }
            }
            ListItem.Standard {
                id: sentrySwitch
                text: i18n.tr("Sentry Mode")
                control: Switch {
                    id: sentrySwitchCheck
                    checked: httpData.vehicle.sentry_mode
                    onTriggered: {
                        const cmd = httpData.vehicle.sentry_mode ?
                                    Api.CONTROL_COMMANDS.disableSentryMode :
                                    Api.CONTROL_COMMANDS.enableSentryMode;
                        commandCall(cmd, () => {
                            checked = httpData.vehicle.sentry_mode;
                        })
                    }
                }
            }
            ListItem.Standard {
                id: chargingSwitch
                text: i18n.tr("Charging")
                control: Switch {
                    id: chargingSwitchCheck
                    enabled: !(httpData.battery.charging_state != "Charging" && httpData.battery.charging_state != "Stopped")
                    checked: httpData.battery.charging_state == "Charging"
                    onTriggered: {
                        const cmd = httpData.battery.charging_state == "Charging" ?
                                    Api.CONTROL_COMMANDS.stopCharging :
                                    Api.CONTROL_COMMANDS.startCharging;
                        commandCall(cmd, () => {
                            checked = httpData.battery.charging_state == "Charging";
                        })
                    }
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Open trunk"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.openTrunk)
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Open frunk"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.openFrunk)
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Vent windows"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.ventWindows)
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Close Windows"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.closeWindows)
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Honk"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.honkHorn)
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Flash"
                    width: parent.width
                    onClicked: commandCall(Api.CONTROL_COMMANDS.flashLights)
                }
            }
        }
    }
}
