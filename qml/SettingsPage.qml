/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.10
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Qt.labs.settings 1.0
import Ubuntu.Components.Pickers 1.0

import "./components"

import "js/api.js" as Api

Page{
    id: settingsPage
    anchors.fill: parent

    property var mainPage;

    property alias apikey: settings.apikey;
    property alias vehicleId: settings.vehicleId;
    property alias metric: settings.metric;
    property alias celsius: settings.celsius;

    Binding {
        target: mainPage
        property: "apikey"
        value: apikey
    }

    Binding {
        target: mainPage
        property: "vehicleId"
        value: vehicleId
    }

    Binding {
        target: mainPage
        property: "metric"
        value: metric
    }

    Binding {
        target: mainPage
        property: "celsius"
        value: celsius
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')
        flickable: flickable
        z: 2

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: "Back"
                onTriggered: {
                    mainPage.update("loading");
                    removePages(settingsPage);
                }
            }
        ]
    }

    TopPopdown {
        id: topPopdown 
    }

    Settings {
        id: settings
        property alias apikey: apikeyTextField.text;
        property alias vehicleId: vehicleIdTextField.text;
        property var metric;
        property var celsius;
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > root.height) ? Flickable.DragAndOvershootBounds : Flickable.StopAtBounds
        /* Set the direction to workaround https://bugreports.qt-project.org/browse/QTBUG-31905
           otherwise the UI might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        Column {
            anchors.left: parent.left
            anchors.right: parent.right

            ItemTitle {
                id: settingsItem
                objectName: "settingsItem"
                text: i18n.tr("Teslascope API:")
            }

            StandardItem {
                text: "API Key"
                TextField {
                    id: apikeyTextField
                    placeholderText: "API Key"
                }
            }

            StandardItem {
                text: "Vehicle Id"
                TextField {
                    id: vehicleIdTextField
                    placeholderText: "Vehicle Id"
                }
            }

            ListItem.SingleControl {
                control: Button {
                    text: "Test API key and Vehicle Id"
                    width: parent.width
                    onClicked: {
                        const api = new Api.Vehicle(settings.apikey, settings.vehicleId);
                        api.state((a) => {
                            print(JSON.stringify(a))
                            if (a.code == 200)
                                topPopdown.show("API key and Vehicle Id is valid!");
                            else
                                topPopdown.show("API key and Vehicle Id is NOT valid!");
                        });
                    }
                }
            }

            ItemTitle {
                id: distItem
                objectName: "distItem"
                text: i18n.tr("Distance units:")
            }

            ListItem.ItemSelector {
                id: setUnitPref
                objectName: "unitPref"
                model: [ i18n.tr("Metric") , i18n.tr("Imperial")]
                expanded: true
                onSelectedIndexChanged: {
                    settings.metric = (selectedIndex === 0)
                }

                Binding {
                    target: setUnitPref
                    property: "selectedIndex"
                    value: settings.metric ? 0 : 1
                }
            }

            ItemTitle {
                id: tempItem
                objectName: "tempItem"
                text: i18n.tr("Temperature units:")
            }

            ListItem.ItemSelector {
                id: setHeatUnitPref
                objectName: "setHeatUnitPref"
                model: [ i18n.tr("Celsius") , i18n.tr("Fahrenheit")]
                expanded: true
                onSelectedIndexChanged: {
                    settings.celsius = (selectedIndex === 0)
                }

                Binding {
                    target: setHeatUnitPref
                    property: "selectedIndex"
                    value: settings.celsius ? 0 : 1
                }
            }

        }
    }
}
