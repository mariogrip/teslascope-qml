/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.10
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import "./components"

import "js/api.js" as Api

Page{
    id: climatePage
    anchors.fill: parent

    // Set from outside
    property var httpData: false
    property var apikey
    property var vehicleId

    property var vehicleApi: new Api.Vehicle(apikey, vehicleId)

    function commandCall(cmd) {
        state = "loading"
        controlsPage.vehicleApi.command(cmd, (a) => {
            state = "ready"
            topPopdown.show(a.response)
        });
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Climate')
        flickable: flickable
        z: 2

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: "Back"
                onTriggered: removePages(climatePage)
            }
        ]
    }

    TopPopdown {
        id: topPopdown 
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > root.height) ? Flickable.DragAndOvershootBounds : Flickable.StopAtBounds
        /* Set the direction to workaround https://bugreports.qt-project.org/browse/QTBUG-31905
           otherwise the UI might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        Column {
            anchors.left: parent.left
            anchors.right: parent.right

            ListItem.Standard {
                id: adjust
                text: i18n.tr("Climate power")
                control: Switch {
                    id: autoAdjustCheck
                    checked: httpData.climate.is_climate_on
                    onTriggered: {
                        const cmd = httpData.climate.is_climate_on ?
                                    Api.CONTROL_COMMANDS.stopAC :
                                    Api.CONTROL_COMMANDS.startAC;
                        commandCall(cmd)
                    }
                }
            }
        }
    }
}
