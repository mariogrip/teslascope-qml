/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import Ubuntu.Components 1.3

Rectangle {
    id: self
    property bool shown: false

    height: units.gu(5)
    z: 1
    anchors.topMargin: 0
    opacity: 0

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.header.bottom  

    color: UbuntuColors.silk
    
    function show(m) {
        text.text = m;
        self.shown = true;
        timer.restart();
    }

    function hide() {
        self.shown = false;
    }

    Label {
        id: text
        text: ""
        anchors.centerIn: parent
    }

    Timer {
        id: timer
        interval: 4000;
        running: false;
        onTriggered: hide()
    }

    states: [
        State { when: self.shown;
            PropertyChanges {   target: self; opacity: 1.0; anchors.topMargin: 0 }
        },
        State { when: !self.shown;
            PropertyChanges {   target: self; opacity: 0.0; anchors.topMargin: -100 }
        }
    ]

    transitions: Transition {
        NumberAnimation { property: "opacity"; duration: 700}
        NumberAnimation { property: "anchors.topMargin"; duration: 700; easing.type: Easing.InOutCubic}
    }
}
