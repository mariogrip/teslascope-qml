/*
 * Copyright (C) 2021  Marius Gripsgard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * teslascope is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    property alias text: label.text
    anchors {
        left: parent.left
        right: parent.right
    }
    height: units.gu(6)

    Label {
        id: label
        anchors {
            top: parent.top
            topMargin: units.gu(3)
            right: parent.right
            rightMargin: units.gu(2)
            bottom: parent.bottom
            left: parent.left
            leftMargin: units.gu(2)
        }
        fontSize: "medium"
        opacity: 0.75
    }
}
